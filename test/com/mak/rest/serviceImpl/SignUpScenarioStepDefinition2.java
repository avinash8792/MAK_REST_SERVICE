package com.mak.rest.serviceImpl;

import static org.junit.Assert.*;


import java.math.BigInteger;
import java.text.DecimalFormat;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.mak.rest.*;
import com.mak.rest.dao.*;
import com.mak.rest.entity.UserBean;

public class SignUpScenarioStepDefinition2 {
	private String firstName;
	private String lastName;
	private String mobile;

	private String userName;
	private String password;
	private UserDetailsDao userDetailsDao;
	private boolean actual ,expected;
	private UserBean userBean;


@Given("^a New T-mobile user  tries to signup$")
public void a_New_T_mobile_user_tries_to_signup() throws Throwable {
	userDetailsDao = new UserDetailsDao();
	
   
}

@When("^he provides \"([^\"]*)\", \"([^\"]*)\" ,\"([^\"]*)\" , \"([^\"]*)\"and \"([^\"]*)\"$")
public void he_provides_and(String arg1, String arg2, String arg3, String arg4, String arg5) throws Throwable {
	firstName= arg1;
	lastName= arg2;
	userName= arg3;
	mobile= arg4;
	password= arg5;
	userBean = new UserBean(firstName,lastName,userName,mobile,password);
}

@Then("^verify if he is a registered or not$")
public void verify_if_he_is_a_registered_or_not() throws Throwable {
	actual =userDetailsDao.addUser(userBean);
}

@Then("^return new user or not  \"([^\"]*)\"$")
public void return_new_user_or_not(String arg1) throws Throwable {
	expected= Boolean.valueOf(arg1);
	assertEquals(expected,actual);
    
}


}