package com.mak.rest.serviceImpl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.mak.rest.dao.UserDetailsDao;
import com.mak.rest.entity.UserBean;
import com.mak.rest.service.UserService;

public class ServiceImplTest {
	private UserService userServiceImpl;
	private String userName;
	private String password;
	private UserBean user;


	@Before
	public void setUp() throws Exception {
		 userServiceImpl = new UserServiceImpl() ;
	}

	@Test
	public void testValidateUserLoginDetails() {
		userName="valid1";
		password= "valid1";
		boolean actual= userServiceImpl.validateUserLoginDetails(userName, password);
		assertTrue(actual);
	}
	@Test
	public void testAddUserDetails() {
		 user = new UserBean("add2","add2","add2","12345","add2");
		 boolean actual= userServiceImpl.addUserDetails(user);
		assertTrue(actual);
	}
	
	@Test
	public void testGetUserDetails() {
		user = userServiceImpl.getUserDetails("valid1");
		String expected = "valid1";
		String actual=user.getPassword();
		assertEquals(expected,actual);
	}
	
	@Test
	public void testUpdateAccount() {
		user = new UserBean("update2","update2","update2","1235","update2");
		boolean updated = userServiceImpl.updateUserDetails(user);
		assertTrue(updated);
	}
	
	@Test
	public void testDeleteUser() {
		boolean actual = userServiceImpl.deleteUser("delete2");
		assertEquals(true,actual);
	}

}
