package com.mak.rest.serviceImpl;

import static org.junit.Assert.*;


import java.math.BigInteger;
import java.text.DecimalFormat;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.mak.rest.*;
import com.mak.rest.dao.*;

public class LoginScenarioStepDefinition {
	private String userName;
	private String password;
	private UserDetailsDao userDetailsDao;
	private boolean actual ,expected;

@Given("^a T-mobile user who tries to login$")
public void a_T_mobile_user_who_tries_to_login() throws Throwable {
	userDetailsDao = new UserDetailsDao();
}

@When("^he provides \"([^\"]*)\" and \"([^\"]*)\"$")
public void he_provides_and(String arg1, String arg2) throws Throwable {
	userName= arg1;
	password= arg2;
}

@Then("^validate his credentials$")
public void validate_his_credentials() throws Throwable {
	actual=userDetailsDao.validateUserLoginDetails(userName, password);
}

@Then("^return user is valid or not \"([^\"]*)\"$")
public void return_user_is_valid_or_not(String arg1) throws Throwable {
	expected= Boolean.valueOf(arg1);
	assertEquals(expected,actual);
}

}