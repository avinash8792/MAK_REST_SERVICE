package com.mak.rest.entity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class UserBeanTest {
	private UserBean user;

	@Before
	public void setUp() throws Exception {
		user = new UserBean("Kavya", "Manda", "kavya@gmail.comas", "6124044159", "kavyamas");

	}

	@Test
	public void testGetFirstName() {
		String actual = user.getFirstName();
		String expected = "Kavya";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetLastName() {
		String actual = user.getLastName();
		String expected = "Manda";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetUserName() {
		String actual = user.getUserName();
		String expected = "kavya@gmail.comas";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetMobile() {
		String actual = user.getMobile();
		String expected = "6124044159";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetPassword() {
		String actual = user.getPassword();
		String expected = "kavyamas";
		assertEquals(expected, actual);
	}

}
