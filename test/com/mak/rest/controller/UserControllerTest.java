package com.mak.rest.controller;

import static org.junit.Assert.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.mak.rest.entity.UserBean;


public class UserControllerTest {
	private static Client client;
	private WebTarget webtarget ;
	private static String REST_SERVICE_URL ;
	private Invocation.Builder invocationBuilder;
	
	@Test
	public void testValidateUserLoginDetails() {
		   Client client = ClientBuilder.newClient();
			  
		   WebTarget webTarget = client.target("http://localhost:8090/MAK_REST_SERVICE/User/validate");
		   Invocation.Builder invocationBuilder = webTarget
				                                  .queryParam("userName", "valid1")
				                                  .queryParam("password", "valid1")
				                                  .request();
		   Response response = invocationBuilder.get();  
		   boolean valid = response.readEntity(Boolean.class);
		   assertTrue(valid);
	}

	@Test
	public void testAddUser() {
		UserBean user = new UserBean("add1", "add1", "add1", "1234", "add1");
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8090/MAK_REST_SERVICE/User/adduser");	
		Response response1 = webTarget.request(MediaType.TEXT_PLAIN).post(Entity.entity(user, MediaType.APPLICATION_JSON));
		boolean addedUser = response1.readEntity(Boolean.class);
		assertTrue(addedUser);
		
	}
	
	@Test
	public void testUpdateUser() {
		UserBean user = new UserBean("update1", "update1", "update1", "1235", "update1");
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8090/MAK_REST_SERVICE/User/updateuser");
		Invocation.Builder invocationBuilder = webTarget
				                               .request();
		Response response = invocationBuilder.put(Entity.entity(user, MediaType.APPLICATION_JSON));
		String result = response.readEntity(String.class);
		boolean updated = Boolean.valueOf(result);
		assertTrue(updated);
		
	}
	
	@Test
	public void testDeleteUser() {

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8090/MAK_REST_SERVICE/User/deleteuser")
				                     .queryParam("userName","delete1");
		Invocation.Builder invocationBuilder = webTarget.request();
		
		Response response = invocationBuilder.delete();
		
		boolean deleted = response.readEntity(Boolean.class);
		assertTrue(deleted);
	}
	

}
