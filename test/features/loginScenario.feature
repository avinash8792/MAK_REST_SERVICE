Feature: UserLogin
user logs in providing username and password, then the details would be validated and returns true if the user is valid user
and returns false if the userdetails are invalid.

Scenario Outline: 
	Given a T-mobile user who tries to login
	When he provides "<userName>" and "<password>"
	Then validate his credentials 
	And return user is valid or not "<validUser>"

	Examples:
	| userName | password | validUser |         
	|     valid1| valid1      | true     |
	|     avi2 | president| false     |     
	 

 