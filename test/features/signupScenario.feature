Feature: UserLogin
New user logs in providing firstname, lastname ,username, mobile and password.Checks if the user is already registered 
and returns true if the user is new user
and returns false if the user is already registered.

Scenario Outline: 
	Given a New T-mobile user  tries to signup
	When he provides "<firstName>", "<lastName>" ,"<userName>" , "<mobile>"and "<password>"
	Then verify if he is a registered or not
	And return new user or not  "<newUser>"

	Examples:
	|firstName |lastName  |userName | mobile    |password 	| newUser |  
	|  	valid1 |   valid1 |  valid1 | 2637863238| valid1	| false	  | 
	|   add3   |   add3   |  add3   | 2637863239| add3	    | true	  | 
	  
	 

 