package com.mak.rest.serviceImpl;

import com.mak.rest.dao.UserDetailsDao;
import com.mak.rest.entity.UserBean;
import com.mak.rest.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDetailsDao userDetailsDao = new UserDetailsDao();


	@Override
	public boolean validateUserLoginDetails(String userName, String password) {

		boolean validUser = userDetailsDao.validateUserLoginDetails(userName, password);
		return validUser;
	}

	@Override
	public boolean addUserDetails(UserBean user) {
		boolean addedUser = userDetailsDao.addUser(user);
		return addedUser;
	}

	@Override
	public UserBean getUserDetails(String userName) {
	
		UserBean user = userDetailsDao.getUserDetails(userName);
		return user;
	}

	@Override
	public boolean updateUserDetails(UserBean user) {
		
		boolean updated = userDetailsDao.updateUserDetails(user);
		return updated;
	}

	@Override
	public boolean deleteUser(String userName) {
		
		boolean deleted = userDetailsDao.deleteUserAccount(userName);
		return deleted;
	}


}
