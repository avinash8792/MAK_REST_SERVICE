package com.mak.rest.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mak.rest.entity.UserBean;
import com.mak.rest.service.UserService;
import com.mak.rest.serviceImpl.UserServiceImpl;

@Path("/User")
public class UserController {
	private UserService userService = new UserServiceImpl();

	// To validate User Credentials
	@GET
	@Path("/validate")
	
	public Response validateUserLoginDetails(@QueryParam("userName") String userName,
			@QueryParam("password") String password) {
		boolean valid = userService.validateUserLoginDetails(userName, password);
		return Response.status(200).entity(valid).build();
	}
	
	// To fetch user details into profile page
	@GET
	@Path("/getUser")
	public Response getUserDetails(@QueryParam("userName") String userName) {
		
		UserBean user = userService.getUserDetails(userName);
		
		return Response.status(200).entity(user).build();
	}
	
	
	// To add User Details to the database
	@POST
	@Path("/adduser")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUser(UserBean userBean) {
		
		boolean addedUser = userService.addUserDetails(userBean);
		
		return Response.status(201).entity(addedUser).build();
	}
	
	// To update User Details
	@PUT
	@Path("/updateuser")
	@Consumes(MediaType.APPLICATION_JSON)	
	public Response updateUserDetails(UserBean userBean) {
		
		boolean updateUser = userService.updateUserDetails(userBean);
		return Response.status(201).entity(String.valueOf(updateUser)).build();
		
		
	}
	
	//To Delete a user
	@DELETE
	@Path("/deleteuser")
	public Response deleteUser(@QueryParam("userName") String userName) {
		boolean deleted = userService.deleteUser(userName);
		return Response.status(200).entity(deleted).build();
	}
}
