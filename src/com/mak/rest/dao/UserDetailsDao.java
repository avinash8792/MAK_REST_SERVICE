package com.mak.rest.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.mak.rest.entity.UserBean;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class UserDetailsDao {

	private String databaseFile = "UserDetails.csv";
	String home = System.getProperty("user.home");
	//private String filePath = home + "/Downloads/" + databaseFile;
	private String filePath = "C:/Users/asanka/" + "/Downloads/" + databaseFile;
	
	private File dataFile = new File(filePath);
	private boolean fileAlreadyExist = dataFile.exists();

	public boolean addUser(UserBean userBean) {
		
		boolean addedUser = false;
		boolean userExist = validateUserLoginDetails(userBean.getUserName(), userBean.getPassword());
		
		if (!userExist) {

			try {
             
				CsvWriter csvWriter = new CsvWriter(new FileWriter(dataFile, true), ',');

				if (!fileAlreadyExist) {

					csvWriter.write("FirstName");
					csvWriter.write("LastName");
					csvWriter.write("UserName");
					csvWriter.write("Mobile");
					csvWriter.write("Password");
					csvWriter.endRecord();
				}

				csvWriter.write(userBean.getFirstName());
				csvWriter.write(userBean.getLastName());
				csvWriter.write(userBean.getUserName());
				csvWriter.write(userBean.getMobile());
				csvWriter.write(userBean.getPassword());
				csvWriter.endRecord();
				csvWriter.close();

				addedUser = true;
				


			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return addedUser;

	}

	public boolean validateUserLoginDetails(String userName, String password) {

		boolean valid = false;
		try {
			CsvReader csvReader = new CsvReader(new FileReader(dataFile));
			csvReader.readHeaders();
			while (csvReader.readRecord()) {
				if (csvReader.get("UserName").equals(userName) && csvReader.get("Password").equals(password)) {
					valid = true;
				}
			}

			csvReader.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return valid;
	}

	public UserBean getUserDetails(String userName) {
		UserBean user = new UserBean();

		try {
			CsvReader csvReader = new CsvReader(new FileReader(dataFile));

			csvReader.readHeaders();
			while (csvReader.readRecord()) {

				if (csvReader.get("UserName").equals(userName)) {

					user.setFirstName(csvReader.get("FirstName"));
					user.setLastName(csvReader.get("LastName"));
					user.setMobile(csvReader.get("Mobile"));
					user.setUserName(csvReader.get("UserName"));
					user.setPassword(csvReader.get("Password"));

				}
			}
			csvReader.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return user;

	}

	public boolean updateUserDetails(UserBean user) {
		boolean updated = false;	
		boolean deleted = deleteUserAccount(user.getUserName());
		boolean added = addUser(user);
		
		if (deleted && added) {
			updated = true;
		}
		return updated;

	}

	public boolean deleteUserAccount(String userName) {
		boolean deleted = false;
		
		try {
			CSVReader csvReader = new CSVReader(new FileReader(dataFile));
			List<String[]> allElements = csvReader.readAll();
			String[] nextLine;
			 for(int i=0;i<allElements.size();i++) {	
			   nextLine =allElements.get(i);
			   if(nextLine[2].equals(userName)) {
						allElements.remove(i);
						deleted = true;
					}
			 }			 
			 CSVWriter writer = new CSVWriter(new FileWriter(dataFile));
			 writer.writeAll(allElements);
			 writer.close();
			 csvReader.close();		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return deleted;
		
	}

	

}
