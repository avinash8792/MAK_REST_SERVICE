package com.mak.rest.service;

import com.mak.rest.entity.UserBean;

public interface UserService {

	//public String addUserDetails(UserBean user);

	public boolean validateUserLoginDetails(String userName, String password);
	
	public boolean addUserDetails(UserBean user);
	
	public UserBean getUserDetails(String userName);
	
	public boolean updateUserDetails(UserBean user);
	
	public boolean deleteUser(String userName);
	
}
