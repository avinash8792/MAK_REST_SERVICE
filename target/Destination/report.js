$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("loginScenario.feature");
formatter.feature({
  "line": 1,
  "name": "UserLogin",
  "description": "user logs in providing username and password, then the details would be validated and returns true if the user is valid user\r\nand returns false if the userdetails are invalid.",
  "id": "userlogin",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 5,
  "name": "",
  "description": "",
  "id": "userlogin;",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 6,
  "name": "a T-mobile user who tries to login",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "he provides \"\u003cuserName\u003e\" and \"\u003cpassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "validate his credentials",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "return user is valid or not \"\u003cvalidUser\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 11,
  "name": "",
  "description": "",
  "id": "userlogin;;",
  "rows": [
    {
      "cells": [
        "userName",
        "password",
        "validUser"
      ],
      "line": 12,
      "id": "userlogin;;;1"
    },
    {
      "cells": [
        "valid1",
        "valid1",
        "true"
      ],
      "line": 13,
      "id": "userlogin;;;2"
    },
    {
      "cells": [
        "avi2",
        "president",
        "false"
      ],
      "line": 14,
      "id": "userlogin;;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 13,
  "name": "",
  "description": "",
  "id": "userlogin;;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 6,
  "name": "a T-mobile user who tries to login",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "he provides \"valid1\" and \"valid1\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "validate his credentials",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "return user is valid or not \"true\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.match({
  "location": "LoginScenarioStepDefinition.a_T_mobile_user_who_tries_to_login()"
});
formatter.result({
  "duration": 1449668484,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "valid1",
      "offset": 13
    },
    {
      "val": "valid1",
      "offset": 26
    }
  ],
  "location": "LoginScenarioStepDefinition.he_provides_and(String,String)"
});
formatter.result({
  "duration": 17625848,
  "status": "passed"
});
formatter.match({
  "location": "LoginScenarioStepDefinition.validate_his_credentials()"
});
formatter.result({
  "duration": 86615517,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 29
    }
  ],
  "location": "LoginScenarioStepDefinition.return_user_is_valid_or_not(String)"
});
formatter.result({
  "duration": 36052166,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "",
  "description": "",
  "id": "userlogin;;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 6,
  "name": "a T-mobile user who tries to login",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "he provides \"avi2\" and \"president\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "validate his credentials",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "return user is valid or not \"false\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.match({
  "location": "LoginScenarioStepDefinition.a_T_mobile_user_who_tries_to_login()"
});
formatter.result({
  "duration": 492365,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "avi2",
      "offset": 13
    },
    {
      "val": "president",
      "offset": 24
    }
  ],
  "location": "LoginScenarioStepDefinition.he_provides_and(String,String)"
});
formatter.result({
  "duration": 484058,
  "status": "passed"
});
formatter.match({
  "location": "LoginScenarioStepDefinition.validate_his_credentials()"
});
formatter.result({
  "duration": 3983477,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 29
    }
  ],
  "location": "LoginScenarioStepDefinition.return_user_is_valid_or_not(String)"
});
formatter.result({
  "duration": 359834,
  "status": "passed"
});
formatter.uri("signupScenario.feature");
formatter.feature({
  "line": 1,
  "name": "UserLogin",
  "description": "New user logs in providing firstname, lastname ,username, mobile and password.Checks if the user is already registered \r\nand returns true if the user is new user\r\nand returns false if the user is already registered.",
  "id": "userlogin",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 6,
  "name": "",
  "description": "",
  "id": "userlogin;",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "a New T-mobile user  tries to signup",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "he provides \"\u003cfirstName\u003e\", \"\u003clastName\u003e\" ,\"\u003cuserName\u003e\" , \"\u003cmobile\u003e\"and \"\u003cpassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify if he is a registered or not",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "return new user or not  \"\u003cnewUser\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "userlogin;;",
  "rows": [
    {
      "cells": [
        "firstName",
        "lastName",
        "userName",
        "mobile",
        "password",
        "newUser"
      ],
      "line": 13,
      "id": "userlogin;;;1"
    },
    {
      "cells": [
        "valid1",
        "valid1",
        "valid1",
        "2637863238",
        "valid1",
        "false"
      ],
      "line": 14,
      "id": "userlogin;;;2"
    },
    {
      "cells": [
        "add3",
        "add3",
        "add3",
        "2637863239",
        "add3",
        "true"
      ],
      "line": 15,
      "id": "userlogin;;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 14,
  "name": "",
  "description": "",
  "id": "userlogin;;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "a New T-mobile user  tries to signup",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "he provides \"valid1\", \"valid1\" ,\"valid1\" , \"2637863238\"and \"valid1\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify if he is a registered or not",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "return new user or not  \"false\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({
  "location": "SignUpScenarioStepDefinition2.a_New_T_mobile_user_tries_to_signup()"
});
formatter.result({
  "duration": 666808,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "valid1",
      "offset": 13
    },
    {
      "val": "valid1",
      "offset": 23
    },
    {
      "val": "valid1",
      "offset": 33
    },
    {
      "val": "2637863238",
      "offset": 44
    },
    {
      "val": "valid1",
      "offset": 60
    }
  ],
  "location": "SignUpScenarioStepDefinition2.he_provides_and(String,String,String,String,String)"
});
formatter.result({
  "duration": 1410641,
  "status": "passed"
});
formatter.match({
  "location": "SignUpScenarioStepDefinition2.verify_if_he_is_a_registered_or_not()"
});
formatter.result({
  "duration": 14817025,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 25
    }
  ],
  "location": "SignUpScenarioStepDefinition2.return_new_user_or_not(String)"
});
formatter.result({
  "duration": 532767,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "",
  "description": "",
  "id": "userlogin;;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "a New T-mobile user  tries to signup",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "he provides \"add3\", \"add3\" ,\"add3\" , \"2637863239\"and \"add3\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify if he is a registered or not",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "return new user or not  \"true\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({
  "location": "SignUpScenarioStepDefinition2.a_New_T_mobile_user_tries_to_signup()"
});
formatter.result({
  "duration": 517286,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "add3",
      "offset": 13
    },
    {
      "val": "add3",
      "offset": 21
    },
    {
      "val": "add3",
      "offset": 29
    },
    {
      "val": "2637863239",
      "offset": 38
    },
    {
      "val": "add3",
      "offset": 54
    }
  ],
  "location": "SignUpScenarioStepDefinition2.he_provides_and(String,String,String,String,String)"
});
formatter.result({
  "duration": 760448,
  "status": "passed"
});
formatter.match({
  "location": "SignUpScenarioStepDefinition2.verify_if_he_is_a_registered_or_not()"
});
formatter.result({
  "duration": 91021507,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 25
    }
  ],
  "location": "SignUpScenarioStepDefinition2.return_new_user_or_not(String)"
});
formatter.result({
  "duration": 349262,
  "status": "passed"
});
});